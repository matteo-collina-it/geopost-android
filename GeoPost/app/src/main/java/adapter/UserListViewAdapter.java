package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.macbook.geopost.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.User;

/**
 * Created by MacBook on 15/12/17.
 */

public class UserListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<User> userList = new ArrayList<>();
    private Boolean error = false;


    public UserListViewAdapter(Context context, ArrayList<User> userList) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.userList.addAll(userList);
    }

    @Override
    public int getCount() {
        return !error ? userList.size() : 1;
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.simple_list_user_1, parent, false);
        }
        TextView textView = (TextView) view.findViewById(R.id.mainText);
        ImageView icon = (ImageView) view.findViewById(R.id.icon);
        if (!error) {
            textView.setText(userList.get(position).getName());
            icon.setImageResource(R.drawable.ic_account_circle);
        }else{
            textView.setText(this.mContext.getString(R.string.error_download_data));
            icon.setImageResource(R.drawable.ic_error);
        }
        return view;
    }
    public void changeDataWithList(ArrayList<User> userList) {
        this.error = false;
        this.userList.clear();
        this.userList.addAll(userList);
        notifyDataSetChanged();
    }
    public void setErrorOnList(){
        this.error = true;
        notifyDataSetChanged();
    }

}
