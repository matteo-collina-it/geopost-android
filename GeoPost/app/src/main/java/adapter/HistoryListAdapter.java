package adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.macbook.geopost.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import model.History;
import model.User;

/**
 * Created by MacBook on 15/12/17.
 */

public class HistoryListAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<History> historyList = new ArrayList<>();
    private Boolean error = false;

    public HistoryListAdapter(Context context) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
    }

    public HistoryListAdapter(Context context, ArrayList<History> historyList) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.historyList.addAll(historyList);
    }

    @Override
    public int getCount() {
        return !error ? historyList.size() : 1;
    }

    @Override
    public History getItem(int position) {
        return historyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.simple_history_cell, parent, false);
        }
        History history = historyList.get(position);
        TextView valueTextView = (TextView) view.findViewById(R.id.valueText);
        TextView dateTextView = (TextView) view.findViewById(R.id.dateText);
        valueTextView.setText(history.getValue());
        dateTextView.setText(history.getDate());
        return view;
    }
    public void changeDataWithList(ArrayList<History> list) {
        this.error = false;
        this.historyList.clear();
        this.historyList.addAll(list);
        notifyDataSetChanged();
    }

}