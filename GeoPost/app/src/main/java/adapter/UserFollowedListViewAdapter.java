package adapter;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.macbook.geopost.R;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import model.History;
import model.User;
import model.Utility;

/**
 * Created by MacBook on 15/12/17.
 */

public class UserFollowedListViewAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<User> userList = new ArrayList<>();
    private Location fromLocation = new Location("");


    public UserFollowedListViewAdapter(Context context) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public User getItem(int position) {
        return userList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if( view == null ){
            view = inflater.inflate(R.layout.simple_list_user_followed_1, parent, false);
        }
        User user = userList.get(position);
        TextView mainText = (TextView)view.findViewById(R.id.mainText);
        mainText.setText(user.getName());

        TextView detailText = (TextView)view.findViewById(R.id.detailText);

        if(this.fromLocation != null){
            detailText.setText(Utility.fromMeterToKm(this.fromLocation.distanceTo(user.getLocation())));
        }else{
            detailText.setText("N.A");
        }

        TextView longText = (TextView)view.findViewById(R.id.longText);
        longText.setText(user.getMessageText());
        return view;
    }

    public void changeDataWithList(Location from) {
        changeDataWithList(this.userList,from);
    }
    public void changeDataWithList(ArrayList<User> userList,Location from) {
        this.userList.clear();
        this.userList.addAll(userList);
        this.fromLocation.set(from);

        Collections.sort(this.userList, new Comparator<User>() {
            @Override
            public int compare(User u1, User u2) {
                float dist1 = fromLocation.distanceTo(u1.getLocation());
                float dist2 = fromLocation.distanceTo(u2.getLocation());
                return Float.compare(dist1,dist2);
            }
        });
        notifyDataSetChanged();
    }


}
