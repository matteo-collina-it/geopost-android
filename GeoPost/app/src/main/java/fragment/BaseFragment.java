package fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.macbook.geopost.BaseActivity;
import com.example.macbook.geopost.R;

import at.grabner.circleprogress.CircleProgressView;

/**
 * Created by MacBook on 17/12/17.
 */

public class BaseFragment extends Fragment {
    View mView;
    CircleProgressView mCircleView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showSpinnerOnView(View v,int timeFade){

        mCircleView.setAlpha(0);
        mCircleView.setVisibility(View.VISIBLE);
        mCircleView.spin();
        mCircleView.animate().alpha(1.0f).setDuration(timeFade).setListener(null);
/*
        v.animate()
                .alpha(0.1f)
                .setDuration(timeFade)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                    }
                });*/
        mCircleView.bringToFront();
    }
}