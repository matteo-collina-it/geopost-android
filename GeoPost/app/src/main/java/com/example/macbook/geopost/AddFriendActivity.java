package com.example.macbook.geopost;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import adapter.UserListViewAdapter;
import at.grabner.circleprogress.CircleProgressView;
import model.HillDialog;
import model.HillRequest;
import model.HillVolleyError;
import model.RequestManager;
import model.User;
import model.singleton.UserSingleton;

public class AddFriendActivity extends RootMenuActivity implements AdapterView.OnItemClickListener {
    SearchView searchView;
    ListView list;
    UserListViewAdapter adapter;
    ArrayList<User> userList = new ArrayList<>();
    RequestQueue queue;
    HillRequest stringRequest;
    private Boolean mError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.flContent);
        getLayoutInflater().inflate(R.layout.activity_add_friend, contentFrameLayout);
        setCheckedItemMenu(R.id.nav_add_friends_fragment);

        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        searchView = (SearchView)findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getUserWithPrefix(newText);
                return false;
            }
        });
        list = (ListView) findViewById(R.id.listview);
        adapter = new UserListViewAdapter(this,userList);
        list.setOnItemClickListener(this);
        list.setAdapter(adapter);

        actionButton = (FloatingActionButton) findViewById(R.id.btnAction);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qry = searchView.getQuery().toString();
                getUserWithPrefix(qry != "" ? qry : "");
            }
        });
        actionButton.hide();
        getUserWithPrefix("");
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void getUserWithPrefix(String prefix){
        queue = Volley.newRequestQueue(this);
        stringRequest = RequestManager.getListUsers(this,
                prefix,
                new Response.Listener<ArrayList<User>>() {
                    @Override
                    public void onResponse(ArrayList<User> list) {
                        hideSpinnerOnView(findViewById(R.id.root_layout));
                        adapter.changeDataWithList(UserSingleton.getInstance().getListFilteredFollowing(list));
                        actionButton.hide();
                        mError = false;
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideSpinnerOnView(findViewById(R.id.root_layout));
                        HillVolleyError herror = new HillVolleyError(error);
                        actionButton.show();
                        showToastError(herror.isBadRequest() ? getString(R.string.error_generic) : null);
                        mError = true;
                        adapter.setErrorOnList();
                    }
                });
        queue.add(stringRequest);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        closeKeyboard();
        if(!mError) {
            final String username = adapter.getItem(position).getName();
            queue = Volley.newRequestQueue(this);
            stringRequest = RequestManager.followUser(this,
                    username,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            HillDialog.showConfirm(AddFriendActivity.this,
                                    username + " " + getString(R.string.success_add_friend));
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            HillVolleyError herror = new HillVolleyError(error);
                            HillDialog.showError(AddFriendActivity.this, herror.isBadRequest() ?
                                    showContentMessage(herror.getMessage()) : getString(R.string.error_generic));
                        }
                    });
            queue.add(stringRequest);
        }
    }
}
