package com.example.macbook.geopost;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import at.grabner.circleprogress.CircleProgressView;
import model.HillDialog;
import model.HillRequest;
import model.HillEditText;
import model.HillVolleyError;
import model.RequestManager;
import model.SessionManager;
import model.Utility;

public class LoginActivity extends BaseActivity {
    RequestQueue queue;
    SessionManager session;
    HillEditText editTextEmail;
    HillEditText editTextPsw;
    HillRequest stringRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new SessionManager(getApplicationContext());
        editTextEmail = (HillEditText) findViewById(R.id.editTextEmail);
        editTextPsw = (HillEditText) findViewById(R.id.editTextPassword);
        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Utility.CONFIG.DEBUG) {
            Log.d("SessionID", session.getUserDetails().toString());
        }
        if (session.isLoggedIn()) {
            procedeToHome();
        }
    }

    public void touchedBtnLogin(View v) {
        if (Utility.CONFIG.DEBUG || this.isValidForm()) {
            closeKeyboard();
            showSpinnerOnView(findViewById(R.id.root_layout));

            queue = Volley.newRequestQueue(this);
            stringRequest = RequestManager.login(this.getBaseContext(),
                    editTextEmail.getText().toString(),
                    editTextPsw.getText().toString(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            session.createLoginSession(response);
                            procedeToHome();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            HillVolleyError herror = new HillVolleyError(error);
                            HillDialog.showError(LoginActivity.this, herror.isBadRequest() ?
                                    getString(R.string.error_invalid_credentials) : null);
                        }
                    });
            queue.add(stringRequest);
        }
    }

    public void procedeToHome() {
        startActivityForResult(new Intent(getBaseContext(), FollowingActivity.class),GENERAL_REQUEST);
    }

    public Boolean isValidForm() {
        Boolean isValid = true;

        TextInputLayout textInputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        TextInputLayout textInputLayoutPsw = (TextInputLayout) findViewById(R.id.input_layout_password);

        if (editTextEmail.isEmpty()) {
            isValid = false;
            textInputLayoutEmail.setError(getString(R.string.error_input_text));
        } else {
            textInputLayoutEmail.setErrorEnabled(false);
        }
        if (!editTextPsw.isValidPassword()) {
            isValid = false;
            textInputLayoutPsw.setError(getString(R.string.error_psw));
        } else {
            textInputLayoutPsw.setErrorEnabled(false);
        }
        return isValid;
    }
}
