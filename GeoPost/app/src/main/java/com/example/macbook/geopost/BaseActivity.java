package com.example.macbook.geopost;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.grabner.circleprogress.CircleProgressView;
import model.SessionManager;
import model.Utility;
import model.singleton.UserSingleton;

public class BaseActivity extends AppCompatActivity {
    static final int GENERAL_REQUEST = 1;
    public CircleProgressView mCircleView;
    public FloatingActionButton actionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    public void showSpinnerOnView(View v,int timeFade){

        mCircleView.setAlpha(0);
        mCircleView.setVisibility(View.VISIBLE);
        mCircleView.spin();
        mCircleView.animate().alpha(1.0f).setDuration(timeFade).setListener(null);

        v.animate()
                .alpha(0.1f)
                .setDuration(timeFade)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                    }
                });

        setUntouchableScreen();
    }
    public void showSpinnerOnView(View v){
        showSpinnerOnView(v,500);
    }

    public void hideSpinnerOnView(View v,int timeFade){
        mCircleView.animate()
                .alpha(0f)
                .setDuration(timeFade)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mCircleView.stopSpinning();
                        mCircleView.setVisibility(View.GONE);
                    }
                });

        v.animate()
                .alpha(1.0f)
                .setDuration(timeFade)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                    }
                });

        setTouchableScreen();
    }
    public void hideSpinnerOnView(View v){
        hideSpinnerOnView(v,500);
    }

    public void setUntouchableScreen(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    public void setTouchableScreen(){
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    public void closeKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    public static ActivityInfo[] listAllActivities(Context context) {
        PackageManager pManager = context.getPackageManager();
        String packageName = context.getApplicationContext().getPackageName();

        try {
            ActivityInfo[] list = pManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES).activities;
            for (ActivityInfo activityInfo : list) {
                Log.d("Info", "ActivityInfo = " + activityInfo.name + " " + activityInfo.getClass());
            }
            return list;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void showToastError(String message){
        Toast.makeText(BaseActivity.this,
                message == null ? this.getBaseContext().getResources().getString(R.string.error_generic) : message,
                Toast.LENGTH_SHORT).show();
    }

    public static void logout(Context ctx){
        Log.d("BaseActivity","Logout");
        SessionManager session = new SessionManager(ctx.getApplicationContext());
        session.logoutSession();
        UserSingleton.getInstance().reset();
        Intent i = new Intent(ctx, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ctx.startActivity(i);
    }

    public String showContentMessage(String msgServer){
        if (msgServer != null){
            if (msgServer.equals(Utility.ERROR.ALREADY_FOLLOWED)){
                return getString(R.string.error_already_follow);
            }else if(msgServer.equals(Utility.ERROR.FOLLOW_YOURSELF)){
                return getString(R.string.error_add_yourself);
            }else if(msgServer.equals(Utility.ERROR.USERNAME_NOT_FOUND)){
                return getString(R.string.error_username_not_found);
            }else{
                return getString(R.string.error_generic);
            }
        }else{
            return getString(R.string.error_generic);
        }
    }
}
