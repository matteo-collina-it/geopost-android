package com.example.macbook.geopost;

import android.*;
import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import model.HillDialog;
import model.User;
import model.singleton.UserSingleton;

public class RootMapActivity extends RootMenuActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    public static final int LOCATION_PERMISSION = 1;
    public boolean mapReady = false;
    private boolean permissionGranted = false;
    private boolean googleApiClientReady = false;
    private Location userLocation;

    GoogleMap map;
    private LatLngBounds.Builder bounds;
    SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_root_map);

        //map builder
        bounds = new LatLngBounds.Builder();
        userLocation = new Location("");
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Check if GooglePlayServices are available
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if(status == ConnectionResult.SUCCESS) {
            Log.d("RootMapActivity", "GooglePlayServices available");
        } else {
            Log.d("RootMapActivity", "GooglePlayServices UNAVAILABLE");
            if(googleApiAvailability.isUserResolvableError(status)) {
                Log.d("RootMapActivity", "Ask the user to fix the problem");
                //If the user accepts to install the google play services,
                //a new app will open. When the user gets back to this activity,
                //the onStart method is invoked again.
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            } else {
                Log.d("RootMapActivity", "The problem cannot be fixed");
            }
        }

        // Instantiate and connect GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("RootMapActivity", "Map is Ready");
        this.map = googleMap;
        mapReady = true;
        this.map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.window_user_map, null);
                LatLng latLng = marker.getPosition();
                String username = "";
                String message = "";
                if(marker.getTag().getClass() == User.class){
                    User user = (User) marker.getTag();
                    username = user.getName();
                    message = user.getMessageText();
                }else{
                    username = marker.getTitle();
                    message = "Latitude:" + latLng.latitude + " " + "Longitude:"+ latLng.longitude;
                }
                TextView textViewFirst = (TextView) v.findViewById(R.id.username);
                TextView textViewSecond = (TextView) v.findViewById(R.id.message);
                textViewFirst.setText(username);
                textViewSecond.setText(message);
                return v;
            }
        });

        this.map.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[],int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                    enableMyLocation();
                } else {
                    Log.d("RootMapActivity", "User deny permission for position");
                    HillDialog.messageAlert(RootMapActivity.this,getString(R.string.error),getString(R.string.error_no_auth_permission_location));
                }
                return;
            }
        }
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION);
        } else if (this.map != null) {
            permissionGranted = true;
            this.map.setMyLocationEnabled(true);
            this.map.getUiSettings().setZoomControlsEnabled(true);
            this.map.getUiSettings().setMyLocationButtonEnabled(true);
            checkAndStartLocationUpdate();
        }
    }

    //Called when all is OK
    private void checkAndStartLocationUpdate() {
        Log.d("RootMapActivity",permissionGranted && googleApiClientReady && mapReady ? "checkAndStartLocationUpdate" : "NOT CALLED checkAndStartLocationUpdate");
        if (permissionGranted && googleApiClientReady && mapReady) {
            Log.d("RootMapActivity", "Start updating location");
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (SecurityException e) {
                // this should not happen because the exception fires when the user has not
                // granted permission to use location, but we already checked this
                e.printStackTrace();
            }
            this.map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
                @Override
                public boolean onMyLocationButtonClick()
                {
                    centerCameraToUserLocation();
                    return false;
                }
            });
            allIsRunning();
        }
    }

    protected void allIsRunning(){
        Log.d("RootMapActivity","All is running ...");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("RootMapActivity", "GoogleApiClient connected");
        googleApiClientReady = true;
        checkAndStartLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("RootMapActivity", "GoogleApiClient suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("RootMapActivity", "GoogleApiClient failed : " + connectionResult.toString());
        HillDialog.messageAlert(RootMapActivity.this,getString(R.string.error),getString(R.string.error_connection_google_api));
    }

    @Override
    protected void onStop() {
        Log.d("RootMapActivity", "GoogleApiClient stopped");
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.userLocation.set(location);
        Log.d("RootMapActivity","onLocationChanged : " + location.toString());
    }

    public Location getUserLocation(){
        return this.userLocation;
    }
    public Boolean isValidUserLocation(){
        return this.userLocation.getLatitude() == 0 && this.userLocation.getLongitude() == 0 ? false : true;
    }

    public void showUsers(ArrayList<User> list) {
        if (permissionGranted && googleApiClientReady && mapReady) {
            if(list.size() > 0){
                this.map.clear();
                for (int i = 0; i < list.size(); i++) {
                    User user = list.get(i);
                    LatLng position = new LatLng(user.getLatitude(), user.getLongitude());
                    bounds.include(position);
                    this.map.addMarker(new MarkerOptions().position(position).title(user.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.baloon_maps)))
                            .setTag(user);
                }
                centerCamera();
            }
        }
    }
    public void centerCamera(){
        this.map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 0));
    }

    public void centerCameraToLocationAndPinLocation(User user){
        centerCameraToLocation(user.getCoordinate());

        this.map.clear();
        this.map.addMarker(new MarkerOptions().position(user.getCoordinate())
                .title(getResources().getResourceName(R.string.last_position))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.baloon_maps)))
                .setTag(user);
    }
    public void centerCameraToLocation(LatLng latLng){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)      // Sets the center of the map to location user
                .zoom(17)                   // Sets the zoom
                .build();
        this.map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    public void centerCameraToUserLocation(){
        LatLng userLocation = new LatLng(this.getUserLocation().getLatitude(),this.getUserLocation().getLongitude());
        Log.d("UserLocation",userLocation.toString());

        //mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        centerCameraToLocation(userLocation);
    }
}
