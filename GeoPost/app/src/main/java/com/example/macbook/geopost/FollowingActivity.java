package com.example.macbook.geopost;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.support.design.widget.FloatingActionButton;
import android.support.transition.Visibility;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

import adapter.UserFollowedListViewAdapter;
import adapter.UserListViewAdapter;
import at.grabner.circleprogress.CircleProgressView;
import model.HillDialog;
import model.HillRequest;
import model.HillVolleyError;
import model.RequestManager;
import model.User;
import model.Utility;
import model.singleton.UserSingleton;

public class FollowingActivity extends RootMapActivity implements AdapterView.OnItemClickListener {
    RequestQueue queue;
    HillRequest stringRequest;
    RadioButton btn_show_map;
    RadioButton btn_show_list;
    RelativeLayout map_section_layout;
    RelativeLayout list_section_layout;
    ListView list;
    UserFollowedListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.flContent);
        getLayoutInflater().inflate(R.layout.activity_following, contentFrameLayout);
        setCheckedItemMenu(R.id.nav_following_fragment);

        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        actionButton = (FloatingActionButton) findViewById(R.id.btnAction);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadData();
            }
        });
        actionButton.hide();

        //we get a ref to the map fragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        map_section_layout = (RelativeLayout) findViewById(R.id.map_section_layout);
        list_section_layout = (RelativeLayout) findViewById(R.id.list_section_layout);

        btn_show_map = (RadioButton) findViewById(R.id.radio_bnt_map);
        btn_show_list = (RadioButton) findViewById(R.id.radio_bnt_list);
        switchMapList(btn_show_map);

        list = (ListView) findViewById(R.id.listview);
        adapter = new UserFollowedListViewAdapter(this);
        list.setOnItemClickListener(this);
        list.setAdapter(adapter);
        Log.d("FollowingActivity","onCreate");
    }

    /*
    * NB: Override this method is important to not refresh data when rotate the screen
    */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("FollowingActivity","onConfigurationChanged");
    }

    /*
    * When map is ready => try to download data.
    * 1- If internet is off, user can retry
    * 2- When user return on this activity, data will be downloaded every time because there isn't
    * a system to understand when data are different.
    */
    @Override
    protected void allIsRunning(){
        super.allIsRunning();
        Log.d("FollowingActivity","All is running");
        downloadData();
    }

    /*
     * When user is moving, change order of list of following users
    */
    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        adapter.changeDataWithList(UserSingleton.getInstance().getListFollowed(),location);
    }

    private void downloadData(){
        showSpinnerOnView(findViewById(R.id.root_layout));
        queue = Volley.newRequestQueue(this);
        stringRequest = RequestManager.getListUsersFollowed(this,
                new Response.Listener<ArrayList<User>>() {
                    @Override
                    public void onResponse(ArrayList<User> list) {
                        actionButton.hide();
                        hideSpinnerOnView(findViewById(R.id.root_layout));
                        UserSingleton.getInstance().addtoFollowedList(list);
                        Log.d("logfollow FollowingActivity", String.valueOf(list.size()));
                        showUsers(UserSingleton.getInstance().getListFollowed());
                        adapter.changeDataWithList(UserSingleton.getInstance().getListFollowed(),getUserLocation());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideSpinnerOnView(findViewById(R.id.root_layout));
                        HillVolleyError herror = new HillVolleyError(error);
                        showToastError(herror.isBadRequest() ? getString(R.string.error_generic) : null);
                        actionButton.show();
                    }
                });
        queue.add(stringRequest);
    }

    public void switchMapList(View view){
        View radio_btn_container = (View) findViewById(R.id.radio_btn_container);
        closeKeyboard();

        if(view != null){
            if(view == this.btn_show_map){
                onStyleChangeRadioButton(this.btn_show_map,true);
                onStyleChangeRadioButton(this.btn_show_list,false);
                map_section_layout.setVisibility(LinearLayout.VISIBLE);
                list_section_layout.setVisibility(LinearLayout.GONE);
                radio_btn_container.getLayoutParams().width = Utility.fromIntToPixel(this,120);
            }else if(view == this.btn_show_list){
                onStyleChangeRadioButton(this.btn_show_map,false);
                onStyleChangeRadioButton(this.btn_show_list,true);
                map_section_layout.setVisibility(LinearLayout.GONE);
                list_section_layout.setVisibility(LinearLayout.VISIBLE);
                radio_btn_container.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            }
        }
    }
    private void onStyleChangeRadioButton(RadioButton btn,Boolean check){
        if(check){
            btn.setBackgroundColor(getColor(R.color.colorPrimary));
            btn.setTextColor(getColor(R.color.colorAgainst));
        }else{
            btn.setBackgroundColor(getColor(R.color.colorAgainst));
            btn.setTextColor(getColor(R.color.colorPrimaryDark));
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        closeKeyboard();
        final String username = adapter.getItem(position).getName();
        Log.d("FollowingActivity",username);
        Intent intent = new Intent(getApplicationContext(), DetailFriendActivity.class);
        intent.putExtra(Utility.DATA_INTENT.USERNAME, username);
        startActivity(intent);
    }

}
