package com.example.macbook.geopost;

import android.location.Location;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.SupportMapFragment;

import at.grabner.circleprogress.CircleProgressView;
import model.HillDialog;
import model.HillEditText;
import model.HillRequest;
import model.HillVolleyError;
import model.RequestManager;
import model.SessionManager;
import model.User;
import model.Utility;
import model.singleton.UserSingleton;

public class UpdateStatusActivity extends RootMapActivity {

    HillEditText editTextMessage;
    RequestQueue queue;
    SessionManager session;
    HillRequest stringRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.flContent);
        getLayoutInflater().inflate(R.layout.activity_update_status, contentFrameLayout);
        setCheckedItemMenu(R.id.nav_update_status);

        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        editTextMessage = (HillEditText) findViewById(R.id.editTextMessage);

        //we get a ref to the map fragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void allIsRunning(){
        super.allIsRunning();
        centerCameraToUserLocation();
    }
    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        centerCameraToUserLocation();
    }

    public void touchedBtnSendMessage(View v) {
        closeKeyboard();
        if(this.isValidForm()){
            showSpinnerOnView(findViewById(R.id.root_layout));
            queue = Volley.newRequestQueue(this);
            stringRequest = RequestManager.updateStatus(this.getBaseContext(),
                    editTextMessage.getText().toString(),
                    this.getUserLocation(),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            HillDialog.showConfirm(UpdateStatusActivity.this,getString(R.string.success_update_status));
                            resetFields();
                            UserSingleton.getInstance().resetUserData();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            HillDialog.showError(UpdateStatusActivity.this,null);
                        }
                    });
            queue.add(stringRequest);
        }
    }
    public Boolean isValidForm() {
        Boolean isValid = true;
        TextInputLayout textInputLayoutMessage = (TextInputLayout) findViewById(R.id.input_layout_message);

        if (editTextMessage.isEmpty()) {
            isValid = false;
            textInputLayoutMessage.setError(getString(R.string.error_message));
        } else {
            textInputLayoutMessage.setErrorEnabled(false);
        }

        if(!isValidUserLocation()){
            isValid = false;
            HillDialog.showError(UpdateStatusActivity.this,getString(R.string.error_user_location));
        }

        return isValid;
    }
    private void resetFields(){
        this.editTextMessage.setText("");
    }
}
