package com.example.macbook.geopost;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import at.grabner.circleprogress.CircleProgressView;
import model.HillDialog;


public class RootMenuActivity extends BaseActivity {
    private FrameLayout flContent;
    public DrawerLayout mDrawer;
    private Toolbar toolbar;
    public NavigationView navigationView;
    // Make sure to be using android.support.v7.app.ActionBarDrawerToggle version.
    // The android.support.v4.app.ActionBarDrawerToggle has been deprecated.
    public ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root_menu);

        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        flContent = (FrameLayout)findViewById(R.id.flContent);
        // Set a Toolbar to replace the ActionBar.
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Find our drawer view
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawerLayout();
        drawerToggle = setupDrawerToggle();
        navigationView = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(navigationView);

        //Set first activity
        //selectDrawerItem(navigationView.getMenu().getItem(0));

        setupNavigationHeader();
    }

    private void setupNavigationHeader(){
        // Inflate the header view at runtime
        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header);

        //TextView textViewHeader = (TextView) headerLayout.findViewById(R.id.textViewHeader);
        //textViewHeader.setText(getString(R.string.app_name));
    }
    private void setupDrawerLayout(){
        mDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }
            @Override
            public void onDrawerOpened(View drawerView) {

            }
            @Override
            public void onDrawerClosed(View drawerView) {

            }
            @Override
            public void onDrawerStateChanged(int newState) {
                closeKeyboard();
            }
        });
    }
    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, toolbar,
                R.string.navigation_drawer_open,  R.string.navigation_drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Intent intent;

        switch(menuItem.getItemId()) {
            case R.id.nav_following_fragment:
                intent = new Intent(getApplicationContext(), FollowingActivity.class);
                break;
            case R.id.nav_add_friends_fragment:
                intent = new Intent(getApplicationContext(), AddFriendActivity.class);
                break;
            case R.id.nav_update_status:
                intent = new Intent(getApplicationContext(), UpdateStatusActivity.class);
                break;
            case R.id.nav_profile:
                intent = new Intent(getApplicationContext(), ProfileActivity.class);
                break;
            default:
                intent = new Intent(getApplicationContext(), FollowingActivity.class);
                break;
        }

        if(!menuItem.isChecked()) {
            try {
                finish();
                startActivityForResult(intent,GENERAL_REQUEST);
                overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
            } catch (Exception e) {
                HillDialog.showError(RootMenuActivity.this, getString(R.string.error_generic));
            }

            // Highlight the selected item has been done by NavigationView
            menuItem.setChecked(true);
            // Set action bar title
            setTitle(menuItem.getTitle());
            // Close the navigation drawer
        }
        mDrawer.closeDrawers();
    }
    // `onPostCreate` called when activity start-up is complete after `onStart()`
    // NOTE 1: Make sure to override the method with only a single `Bundle` argument
    // Note 2: Make sure you implement the correct `onPostCreate(Bundle savedInstanceState)` method.
    // There are 2 signatures and only `onPostCreate(Bundle state)` shows the hamburger icon.
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void setCheckedItemMenu(@IdRes int resId){
        int pos = -1;
        switch(resId) {
            case R.id.nav_following_fragment:
                pos = 0;
                break;
            case R.id.nav_add_friends_fragment:
                pos = 1;
                break;
            case R.id.nav_update_status:
                pos = 2;
                break;
            case R.id.nav_profile:
                pos = 3;
                break;
            default:
                break;
        }
        MenuItem item = navigationView.getMenu().getItem(pos);
        item.setChecked(true);
        setTitle(item.getTitle());
    }

    @Override
    public void onBackPressed() {
        //If user wants to go back to Login, disable back button
        super.onBackPressed();
    }
}