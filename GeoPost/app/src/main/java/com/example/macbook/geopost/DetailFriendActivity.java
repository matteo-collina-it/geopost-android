package com.example.macbook.geopost;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;

import adapter.HistoryListAdapter;
import adapter.UserFollowedListViewAdapter;
import at.grabner.circleprogress.CircleProgressView;
import model.HillRequest;
import model.HillVolleyError;
import model.History;
import model.RequestManager;
import model.User;
import model.Utility;
import model.singleton.UserSingleton;

public class DetailFriendActivity extends RootMenuActivity {
    public User currUser;
    TextView userTextView;
    RequestQueue queue;
    HillRequest stringRequest;
    ListView list;
    HistoryListAdapter adapter;
    RelativeLayout list_section_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.flContent);
        getLayoutInflater().inflate(R.layout.activity_detail_friend, contentFrameLayout);

        //BACK
        //drawerToggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mCircleView = (CircleProgressView) findViewById(R.id.circleView);
        userTextView = (TextView) findViewById(R.id.userTextView);
        list_section_layout = (RelativeLayout) findViewById(R.id.list_section_layout);


        String username = getIntent().getStringExtra(Utility.DATA_INTENT.USERNAME);
        userTextView.setText(username);
        getHistoryOf(username);

        list = (ListView) findViewById(R.id.listview);
        adapter = new HistoryListAdapter(this);
        list.setAdapter(adapter);
    }



    private void getHistoryOf(String username){
        if(username != null && !username.equals("")) {
            showSpinnerOnView(findViewById(R.id.root_layout));
            queue = Volley.newRequestQueue(this);
            stringRequest = RequestManager.getHistory(this,
                    username,
                    new Response.Listener<ArrayList<History>>() {
                        @Override
                        public void onResponse(ArrayList<History> list) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            adapter.changeDataWithList(list);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideSpinnerOnView(findViewById(R.id.root_layout));
                            HillVolleyError herror = new HillVolleyError(error);
                            showToastError(herror.isBadRequest() || herror.isInvalidSession() ? herror.getMessage() : null);
                        }
                    });
            queue.add(stringRequest);
        }
    }
}
