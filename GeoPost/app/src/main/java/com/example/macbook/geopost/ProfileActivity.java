package com.example.macbook.geopost;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;

import at.grabner.circleprogress.CircleProgressView;
import model.HillDialog;
import model.HillRequest;
import model.HillVolleyError;
import model.RequestManager;
import model.SessionManager;
import model.User;
import model.singleton.UserSingleton;

public class ProfileActivity extends RootMapActivity {
    SessionManager session;
    User currUser;
    RequestQueue queue;
    HillRequest stringRequest;
    TextView usernameTextView;
    TextView messageTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.flContent);
        getLayoutInflater().inflate(R.layout.activity_profile, contentFrameLayout);
        setCheckedItemMenu(R.id.nav_profile);

        session = new SessionManager(getApplicationContext());
        mCircleView = (CircleProgressView) findViewById(R.id.circleView);

        //we get a ref to the map fragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        currUser = new User("");
        usernameTextView = (TextView)findViewById(R.id.usernameTextView);
        messageTextView = (TextView)findViewById(R.id.messageTextView);


        if (!UserSingleton.getInstance().existUserData()) {
            showSpinnerOnView(findViewById(R.id.root_layout));
            getUserData();
        }else{
            currUser = UserSingleton.getInstance().getUserData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void getUserData(){
        queue = Volley.newRequestQueue(this);
        stringRequest = RequestManager.getUserData(this,
                new Response.Listener<User>() {
                    @Override
                    public void onResponse(User user) {
                        hideSpinnerOnView(findViewById(R.id.root_layout));
                        currUser = user;
                        refreshFields();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideSpinnerOnView(findViewById(R.id.root_layout));
                        HillDialog.showError(ProfileActivity.this, null);
                    }
                });
        queue.add(stringRequest);
    }

    private void refreshFields(){
        usernameTextView.setText(currUser.getName());
        messageTextView.setText(currUser.getMessageText());
        centerCameraToLocationAndPinLocation(currUser);
    }

    @Override
    protected void allIsRunning(){
        super.allIsRunning();
        refreshFields();
    }
    /*
    MUST BE LAST POSITION
    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        centerCameraToUserLocation();
    }*/

    public void touchedBtnLogout(View v){
        HillDialog.yesNodecisionAlert(ProfileActivity.this,
                getString(R.string.attention),
                getString(R.string.question_logout),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BaseActivity.logout(ProfileActivity.this);
                    }
                });
    }
}
