package model.singleton;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.User;

/**
 * Created by MacBook on 15/12/17.
 */

public class UserSingleton {
    private static final UserSingleton ourInstance = new UserSingleton();
    private ArrayList<User> userList = new ArrayList<>();
    private ArrayList<User> userFollowedList = new ArrayList<>();
    private User userData = new User("");

    public static UserSingleton getInstance() {
        return ourInstance;
    }

    private UserSingleton() {
        userList = new ArrayList<>();
        userFollowedList = new ArrayList<>();
        userData = new User("");
    }
    public void reset(){
        userList = new ArrayList<>();
        userFollowedList = new ArrayList<>();
        userData = new User("");
    }

    public int getCount(){
        return userList.size();
    }
    public int getCountFollowed(){
        return userFollowedList.size();
    }

    public ArrayList<User> getList(){
        return userList;
    }
    public ArrayList<User> getListFollowed(){
        return userFollowedList;
    }
    public ArrayList<User> getListFilteredFollowing(ArrayList<User> list){
        Set<User> ad = new HashSet<User>(list);
        Set<User> bd = new HashSet<User>(userFollowedList);
        ad.removeAll(bd);

        ArrayList<User> finalList = new ArrayList<User>();
        finalList.addAll(ad);

        Collections.sort(finalList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        Log.d("logfollow userFollowedList", String.valueOf(userFollowedList.size()));
        Log.d("logfollow list downloaded", String.valueOf(list.size()));
        Log.d("logfollow final", String.valueOf(finalList.size()));
        return finalList;
    }

    public void addtoList(ArrayList<User> list){
        removeAll();
        for (User u:list) {
            this.addUsertoList(u);
        }
    }
    public void addtoFollowedList(ArrayList<User> list){
        removeAllFollowed();
        for (User u:list) {
            this.addUsertoFollowedList(u);
        }
    }

    private void addUsertoList(User u){
        userList.add(u);
    }
    private void addUsertoFollowedList(User u){
        userFollowedList.add(u);
    }

    private void removeAll(){
        userList.clear();
    }
    private void removeAllFollowed(){
        userFollowedList.clear();
    }

    public void setUserData(User u){
        this.userData = u;
    }
    public User getUserData(){
        return this.userData;
    }
    public Boolean existUserData(){
        Log.d("Profile",userData.getName().equals("") || userData == null ? "false existuserdata" : "true existuserdata");
        return userData.getName().equals("") || userData == null ? false : true;
    }
    public void resetUserData(){
        userData = new User("");
    }

}
