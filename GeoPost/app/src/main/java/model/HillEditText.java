package model;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by MacBook on 14/12/17.
 */

public class HillEditText extends android.support.v7.widget.AppCompatEditText {
    public HillEditText(Context context) {
        super(context);
        init();
    }

    public HillEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HillEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        // set your input filter here
    }

    private String getNormalized(){
        return this.getText().toString().trim();
    }
    public boolean isEmpty() {
        String text_normalized = this.getNormalized();
        return text_normalized.isEmpty();
    }
    public boolean isValidEmail() {
        String email_normalized = this.getNormalized();
        return !email_normalized.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(email_normalized).matches();
    }
    public boolean isValidPassword() {
        return !this.getNormalized().isEmpty();
    }

}
