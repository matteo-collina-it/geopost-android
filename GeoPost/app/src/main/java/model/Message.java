package model;

/**
 * Created by MacBook on 19/12/17.
 */

public class Message {
    private String text = "";

    public Message(String text){
        this.text = text;
    }
    public String getText(){
        return this.text;
    }
}
