package model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.macbook.geopost.R;

public class HillDialog {
    private HillDialogType type;


    public static void okButton(Context ctx, String title, String message) {
        showAlertDialog(HillDialogType.Message, ctx, title, message, null,true, ctx.getResources().getString(R.string.ok));
    }
    public static void showImportantError(Context ctx, String message,DialogInterface.OnClickListener posCallback) {
        showAlertDialog(HillDialogType.Error,
                ctx,
                ctx.getResources().getString(R.string.error),
                message == null ? ctx.getResources().getString(R.string.error_generic) : message,
                posCallback,
                true,
                ctx.getResources().getString(R.string.ok));
    }
    public static void showError(Context ctx, String message) {
        showAlertDialog(HillDialogType.Error,
                ctx,
                ctx.getResources().getString(R.string.error),
                message == null ? ctx.getResources().getString(R.string.error_generic) : message,
                null,
                true,
                ctx.getResources().getString(R.string.ok));
    }
    public static void showConfirm(Context ctx, String message) {
        showAlertDialog(HillDialogType.Confirm,
                ctx,
                ctx.getResources().getString(R.string.congratulation),
                message == null ? ctx.getResources().getString(R.string.success_generic) : message,
                null,
                true,
                ctx.getResources().getString(R.string.ok));
    }
    public static void yesNodecisionAlert(Context ctx, String title, String message, DialogInterface.OnClickListener posCallback, String... buttonNames) {
        showAlertDialog(HillDialogType.Decision, ctx, title, message, posCallback,false, ctx.getString(R.string.ok),ctx.getString(R.string.cancel));
    }


    public static void messageAlert(Context ctx, String title, String message) {
        showAlertDialog(HillDialogType.Message, ctx, title, message, null,false, "OK");
    }



    public static void showAlertDialog(HillDialogType alertType, Context ctx, String title, String message, DialogInterface.OnClickListener posCallback, Boolean isCancelable, String... buttonNames) {
        if ( title == null ) title = ctx.getResources().getString(R.string.app_name);
        if ( message == null ) message = "";

        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(isCancelable);


        switch (alertType) {
            case Message:
            case Decision:
                builder.setIcon(android.R.drawable.ic_dialog_info);
            case Error:
                builder.setIcon(R.drawable.ic_error);
                break;
            case Confirm:
                builder.setIcon(R.drawable.ic_check);
                break;
        }

        if(alertType == HillDialogType.Decision){
            builder.setPositiveButton(buttonNames[0], posCallback);
        }

        builder.setNegativeButton(buttonNames [buttonNames.length - 1], new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.create().show();
    }

}


enum HillDialogType {
    Message,
    Error,
    Confirm,
    Decision
}