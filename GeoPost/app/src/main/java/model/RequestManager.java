package model;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.macbook.geopost.LoginActivity;
import com.example.macbook.geopost.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.singleton.UserSingleton;

/**
 * Created by MacBook on 16/12/17.
 */

public class RequestManager {


    public static HillRequest login(Context ctx,final String username,
                             final String psw,
                             final Response.Listener<String> onSuccess,
                             Response.ErrorListener onFail){
        return  new HillRequest(ctx,Request.Method.POST,Utility.URL.SERVICE.LOGIN,onSuccess, onFail) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = super.getParams(); //new HashMap<String, String>();
                params.put(Utility.PARAMS.LOGIN.USERNAME, Utility.CONFIG.DEBUG ? "MatteoCollina" : username);
                params.put(Utility.PARAMS.LOGIN.PASSWORD, Utility.CONFIG.DEBUG ? "Matteo1234" : psw);
                return params;
            }
        };
    }
    public static HillRequest getListUsers(final Context ctx, final String usernamestart,
                                           final Response.Listener<ArrayList<User>> onSuccess,
                                           Response.ErrorListener onFail){
        String urlGet = HillRequest.getUrlWithParamsInQueryString(Utility.URL.SERVICE.LIST_USER,
                        new HashMap<String, String>() {{
                            if(usernamestart != ""){
                                put(Utility.PARAMS.LIST_USER.USERNAMESTART,usernamestart);
                            }
                        }},ctx);
        return new HillRequest(ctx,Request.Method.GET,urlGet,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                            ArrayList<User> list  =  new ArrayList<User>();
                            try{
                                JSONArray jsonArray = new JSONObject(response).getJSONArray(Utility.KEY_RESPONSE.USERNAMES);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    list.add(new User(jsonArray.getString(i)));
                                }
                            } catch (JSONException e) {
                                HillDialog.showError(ctx,null);
                                e.printStackTrace();
                            }
                            UserSingleton.getInstance().addtoList(list);
                            onSuccess.onResponse(list);
                    }
                }, onFail);
    }
    public static HillRequest followUser(final Context ctx, final String username,
                                           final Response.Listener<String> onSuccess,
                                           Response.ErrorListener onFail){
        String urlGet = HillRequest.getUrlWithParamsInQueryString(Utility.URL.SERVICE.FOLLOW,
                new HashMap<String, String>() {{
                    put(Utility.PARAMS.FOLLOW.USERNAME,username);
                }},ctx);
        return new HillRequest(ctx,Request.Method.GET,urlGet,onSuccess,onFail);
    }
    public static HillRequest getListUsersFollowed(final Context ctx,
                                           final Response.Listener<ArrayList<User>> onSuccess,
                                           Response.ErrorListener onFail){
        String urlGet = HillRequest.getUrlWithParamsInQueryString(Utility.URL.SERVICE.FOLLOWED,
                new HashMap<String, String>() {{
                }},ctx);
        return new HillRequest(ctx,Request.Method.GET,urlGet,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<User> list  =  new ArrayList<User>();
                        try{
                            JSONArray jsonArray = new JSONObject(response).getJSONArray(Utility.KEY_RESPONSE.FOLLOWED);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                list.add(new User(new JSONObject(jsonArray.getString(i))));
                            }
                        } catch (JSONException e) {
                            HillDialog.showError(ctx,null);
                            e.printStackTrace();
                        }
                        UserSingleton.getInstance().addtoFollowedList(list);
                        onSuccess.onResponse(list);
                    }
                }, onFail);
    }
    public static HillRequest updateStatus(Context ctx,
                                           final String message,
                                           final Location location,
                                           final Response.Listener<String> onSuccess,
                                           Response.ErrorListener onFail){
        String urlGet = HillRequest.getUrlWithParamsInQueryString(Utility.URL.SERVICE.UPDATE_STATUS,
                new HashMap<String, String>() {{
                    put(Utility.PARAMS.STATUS_UPDATE.MESSAGE,message);
                    put(Utility.PARAMS.STATUS_UPDATE.LATITUDE,Double.toString(location.getLatitude()));
                    put(Utility.PARAMS.STATUS_UPDATE.LONGITUDE,Double.toString(location.getLongitude()));
                }},ctx);
        return  new HillRequest(ctx,Request.Method.GET,urlGet,onSuccess, onFail);
    }
    public static HillRequest getUserData(final Context ctx,
                                                   final Response.Listener<User> onSuccess,
                                                   Response.ErrorListener onFail){
        String urlGet = HillRequest.getUrlWithParamsInQueryString(Utility.URL.SERVICE.PROFILE,
                new HashMap<String, String>() {{
                }},ctx);
        return new HillRequest(ctx,Request.Method.GET,urlGet,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        User user = new User("");
                        try{
                            user = new User(new JSONObject(response));
                        } catch (JSONException e) {
                            HillDialog.showError(ctx,null);
                            e.printStackTrace();
                        }
                        UserSingleton.getInstance().setUserData(user);
                        onSuccess.onResponse(user);
                    }
                }, onFail);
    }
    public static HillRequest getHistory(final Context ctx,
                                                   final String username,
                                                   final Response.Listener<ArrayList<History>> onSuccess,
                                                   Response.ErrorListener onFail){
        String urlGet = HillRequest.getUrlWithParamsInQueryString(Utility.URL.SERVICE.HISTORY,
                new HashMap<String, String>() {{
                    put(Utility.PARAMS.HISTORY.USERNAME,username);
                }},ctx);
        return new HillRequest(ctx,Request.Method.GET,urlGet,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ArrayList<History> list  =  new ArrayList<History>();
                        try{
                            JSONArray jsonArray = new JSONObject(response).getJSONArray(Utility.KEY_RESPONSE.HISTORY);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                list.add(new History(new JSONObject(jsonArray.getString(i))));
                            }
                            Log.d("esameMC",jsonArray.toString());
                            Log.d("esameMC","Numero di post: " + Integer.toString(list.size()));
                        } catch (JSONException e) {
                            HillDialog.showError(ctx,null);
                            e.printStackTrace();
                        }
                        onSuccess.onResponse(list);
                    }
                }, onFail);
    }
}
