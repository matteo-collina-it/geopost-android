package model;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

/**
 * Created by MacBook on 14/12/17.
 */

public class HillVolleyError extends VolleyError {
    VolleyError error;
    public HillVolleyError(VolleyError error){
        this.error = error;
    }
    public Boolean isBadRequest(){
        if (this.error.networkResponse != null &&
                this.error.networkResponse.statusCode == HttpURLConnection.HTTP_BAD_REQUEST) {
            return true;
        }
        return false;
    }
    public Boolean isInvalidSession(){
        if (this.error.networkResponse != null &&
                this.error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
            return true;
        }
        return false;
    }
    public String getMessage(){
        try {
            return new String(error.networkResponse.data,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.d("UnsupportedEncodingException",e.toString());
        }
        return null;
    }
}
