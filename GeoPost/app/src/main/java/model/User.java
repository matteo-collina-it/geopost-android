package model;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

/**
 * Created by MacBook on 15/12/17.
 */

public class User {
    private String username;
    private double latitude = 0;
    private double longitude = 0;
    private Message message = new Message("");

    private static final String KEY_JSON_USERNAME = "username";
    private static final String KEY_JSON_MESSAGE = "msg";
    private static final String KEY_JSON_LATITUDE = "lat";
    private static final String KEY_JSON_LONGITUDE = "lon";

    public User(JSONObject json) throws JSONException {
        this.username = json.get(KEY_JSON_USERNAME) != null ? json.get(KEY_JSON_USERNAME).toString() : "";
        this.message = new Message(!json.isNull(KEY_JSON_MESSAGE) ? json.get(KEY_JSON_MESSAGE).toString() : "");
        this.latitude = !json.isNull(KEY_JSON_LATITUDE) ? json.getDouble(KEY_JSON_LATITUDE) : 0;
        this.longitude = !json.isNull(KEY_JSON_LONGITUDE) ? json.getDouble(KEY_JSON_LONGITUDE) : 0;
    }
    public User(String username,double latitude, double longitude){
        this.username = username;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public User(String username, String msg, Location location){
        this.username = username;
        this.latitude = location.getLatitude();
        this.longitude = this.getLongitude();
        this.message = new Message(msg);
    }
    public User(String username){
        this.username = username;
    }

    public String getName(){
        return this.username;
    }
    public double getLatitude(){
        return this.latitude;
    }
    public double getLongitude(){
        return this.longitude;
    }
    public LatLng getCoordinate(){
        return new LatLng(this.latitude,this.longitude);
    }
    public Location getLocation(){
        Location l = new Location("");
        l.setLatitude(this.latitude);
        l.setLongitude(this.longitude);
        return l;
    }
    public String getMessageText(){ return this.message.getText(); }
    public String getGeoPoint(){
        return Double.toString(this.latitude) + "/" + Double.toString(this.longitude);
    }

    @Override
    public int hashCode() {
        return this.getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User))
            return false;
        return ((User) obj).getName().equals(this.getName());
    }

}
