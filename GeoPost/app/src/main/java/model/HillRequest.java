package model;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.ParseError;
import com.example.macbook.geopost.BaseActivity;
import com.example.macbook.geopost.ProfileActivity;
import com.example.macbook.geopost.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by MacBook on 13/12/17.
 */

public class HillRequest extends StringRequest{
    private Context context;
    private static Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {}
    };
    private static Response.Listener mResponseListener = new Response.Listener() {
        @Override
        public void onResponse(Object response) {}
    };
    private static final int MY_SOCKET_TIMEOUT_MS = 5000;

    public HillRequest(final Context ctx,
                       int method,
                       String url,
                       final Response.Listener<String> listener,
                       Response.ErrorListener errorListener) {
        super(method,getUrl(url),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("onResponse : ",response.toString());
                        mResponseListener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("onErrorResponse : ",error.toString());
                        HillVolleyError herror = new HillVolleyError(error);
                        if(herror.isInvalidSession()){
                            Toast.makeText(ctx,ctx.getString(R.string.error_invalid_session),Toast.LENGTH_LONG).show();
                            BaseActivity.logout(ctx);
                        }else{
                            mErrorListener.onErrorResponse(error);
                        }
                    }
                });
        this.context = ctx;
        this.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mErrorListener = errorListener;
        mResponseListener = listener;
    }

    private static final String getUrl(String url) {
        Log.d("Called url ",Utility.URL.URL_BASE + url);
        return Utility.URL.URL_BASE + url;
    }
    protected static String getUrlWithParamsInQueryString(String url,HashMap<String, String> params,Context ctx){
        Uri.Builder builder = Uri.parse(url).buildUpon();
        for (String key:params.keySet()) {
            builder.appendQueryParameter(key, params.get(key));
        }
        SessionManager session = new SessionManager(ctx);
        if(session.isLoggedIn()){
            builder.appendQueryParameter(Utility.PARAMS.SESSIONID, session.getSessionID());
        }
        return builder.build().toString();
    }

    @Override
    protected Map<String, String> getParams(){
        Map<String, String> params = new HashMap<String, String>();
        SessionManager session = new SessionManager(this.context);
        if(session.isLoggedIn()){
            params.put(Utility.PARAMS.SESSIONID, session.getSessionID());
        }
        return params;
    }


}
