package model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by MacBook on 26/01/18.
 */

public class History {
    private static final String KEY_JSON_VALUE = "value";
    private static final String KEY_JSON_TIMESTAMP = "timestamp";

    private String value = "";
    private String date = "";

    public History(String value, String date){
        this.value = value;
        this.date = date;
    }

    public History(JSONObject json) throws JSONException {
        this.value = json.get(KEY_JSON_VALUE) != null ? json.get(KEY_JSON_VALUE).toString() : "";
        this.date = json.get(KEY_JSON_TIMESTAMP) != null ? json.get(KEY_JSON_TIMESTAMP).toString() : "-";
    }
    public String getValue(){
        return this.value;
    }
    public String getDate(){
        return this.date;
    }
}
