package model;

import android.content.Intent;
import com.example.macbook.geopost.LoginActivity;
import com.example.macbook.geopost.R;
import java.util.HashMap;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = Context.MODE_PRIVATE;

    private static final String PREF_NAME = "UserPreferences";
    public static final String KEY_SESSIONID = "sessionid";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void createLoginSession(String sessionid) {
        if (sessionid != null && sessionid != "" ) {
            editor.putString(KEY_SESSIONID, sessionid);
            editor.commit();
        }
    }
    public void logoutSession() {
        editor.remove(KEY_SESSIONID);
        editor.commit();
        this.checkLogin();
    }

    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        }
    }
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_SESSIONID, pref.getString(KEY_SESSIONID, null));
        return user;
    }
    public String getSessionID(){
        return this.getUserDetails().get(KEY_SESSIONID);
    }
    public void logoutUser() {
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }
    public boolean isLoggedIn() {
        return pref.getString(KEY_SESSIONID, null) == null ? false : true;
    }
}