package model;

import android.content.Context;
import android.location.Location;
import android.util.Log;

/**
 * Created by MacBook on 13/12/17.
 */

public class Utility {
    public class URL {
        public static final String URL_BASE = "https://ewserver.di.unimi.it/mobicomp/geopost/";
        public class SERVICE {
            public static final String LOGIN = "login";
            public static final String LIST_USER = "users";
            public static final String FOLLOW = "follow";
            public static final String FOLLOWED = "followed";
            public static final String UPDATE_STATUS = "status_update";
            public static final String PROFILE = "profile";
            public static final String HISTORY = "history";
        }
    }
    public class PARAMS {
        public static final String SESSIONID = "session_id";
        public static final String LIMIT = "limit";
        public class LOGIN {
            public static final String USERNAME = "username";
            public static final String PASSWORD = "password";
        }
        public class LIST_USER {
            public static final String USERNAMESTART = "usernamestart";
        }
        public class FOLLOW {
            public static final String USERNAME = "username";
        }
        public class STATUS_UPDATE {
            public static final String MESSAGE = "message";
            public static final String LATITUDE = "lat";
            public static final String LONGITUDE = "lon";
        }
        public class HISTORY {
            public static final String USERNAME = "username";
        }
    }
    public static class KEY_RESPONSE {
        public static final String USERNAMES = "usernames";
        public static final String FOLLOWED = "followed";
        public static final String HISTORY = "history";
    }
    public static class CONFIG {
        public static final Boolean DEBUG = false;
    }

    public static int fromIntToPixel(Context ctx, int dps){
        final float scale = ctx.getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;
    }
    public static String fromMeterToKm(float distance){
        float km = distance/1000;
        if(km < 1){
            return String.format("%d", (int)distance) + " m";
        }else{
            return String.format("%.02f", km) + " km";
        }
    }
    public static class ERROR {
        public static final String ALREADY_FOLLOWED = "ALREADY FOLLOWING USER";
        public static final String FOLLOW_YOURSELF = "CANNOT FOLLOW YOURSELF";
        public static final String  USERNAME_NOT_FOUND = "USERNAME NOT FOUND";
    }
    public static class DATA_INTENT {
        public static final String USERNAME = "USERNAME";
    }
}
